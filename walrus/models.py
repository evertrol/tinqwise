from datetime import datetime, timedelta, timezone
from django.db import models
from django.contrib.auth import get_user_model


CEST = timezone(timedelta(hours=1))

User = get_user_model()


class ToDo(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=128)
    completion_date = models.DateTimeField(null=True, blank=True, default=None)

    def __str__(self):
        return self.title

    @property
    def days_since_completion(self, date=None):
        date = date or datetime.now(CEST)
        delta = timedelta(-1)
        if self.completion_date:
            delta = date - self.completion_date
        # Round down (floor)
        return delta.days
