from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework import serializers
from .models import ToDo


User = get_user_model()


class ToDoSerializer(serializers.ModelSerializer):
    # An alternative to the 'days_since_completion' property in the model
    # would be to use a SerializerMethodField

    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    #user_id = serializers.PrimaryKeyRelatedField(many=True)
    # Restrict to only the user_id, so POST requests have less chance
    # of running into an issue.
    # Though matching `user_id` and `username` could be a good check.
    #username = serializers.CharField(source="user.username")

    class Meta:
        model = ToDo
        fields = ['id', 'title', 'completion_date', 'days_since_completion', 'user']
