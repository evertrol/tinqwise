from django.shortcuts import render
from django.http import Http404
from rest_framework import generics, permissions, response, status, viewsets
from rest_framework.decorators import api_view
from .models import ToDo
from .serializers import ToDoSerializer


def todos_per_user(request, pk=None, username=None):
    context = {}

    # Note: it is assumed that pk == 0 and empty usernames do not
    # exist. The former seems to be a safe assumption with Django and
    # the majority of database. Otherwise, use `if pk is not None`.
    if pk:
        # Note: we're not checking whether the user with this primary key actually exists.
        # `user = get_object_or_404(User, pk=pk)` could handle that.
        context['todos'] = ToDo.objects.filter(user__pk=pk)

        # Set the user (as number) explicitly,
        # since we can't rely on the default (possibly anonymous) user in the template
        context['user'] = f'#{pk}'
    elif username:  # select by username
        context['todos'] = ToDo.objects.filter(user__username=username)
        context['user'] = username
    else:
        # Use the logged-in user
        user = request.user
        if user.is_anonymous:
            # Alternatively, return an empty quereyset or list
            raise Http404

        context['todos'] = ToDo.objects.filter(user=user)

    return render(request, 'walrus/todos_per_user.html', context)



class UserOnlyPermission(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


# Note: there is no filtering on the user: anyone can view or add
# notes for any existing user
class ToDoList(generics.ListCreateAPIView):
    serializer_class = ToDoSerializer
    permission_classes = [permissions.IsAuthenticated, UserOnlyPermission]

    def get_queryset(self):
        user = self.request.user
        return ToDo.objects.filter(user=user)

    def create(self, request, *args,**kwargs):
        if request.user.pk != request.data['user']:
             return response.Response(
                 {'detail': 'authenticated user does not match todo owner'} ,
                 status=status.HTTP_406_NOT_ACCEPTABLE)
        return super().create(request, *args, **kwargs)


class ToDoView(generics.RetrieveUpdateDestroyAPIView):
    queryset = ToDo.objects.all()
    serializer_class = ToDoSerializer
    permission_classes = [UserOnlyPermission]


class ToDoViewSet(viewsets.ModelViewSet):
    serializer_class = ToDoSerializer
    permission_classes = [permissions.IsAuthenticated, UserOnlyPermission]

    def get_queryset(self):
        todos = ToDo.objects.filter(user=self.request.user)
        status = self.request.query_params.get('status', '')
        if status.lower() == 'incomplete':
            todos = todos.filter(completion_date__isnull=True)
        elif status.lower() == 'complete':
            todos = todos.filter(completion_date__isnull=False)
        return todos

    # Override the create method for an extra check
    def create(self, request, *args, **kwargs):
        if request.user.pk != request.data['user']:
             return response.Response(
                 {'detail': 'authenticated user does not match todo owner'} ,
                 status=status.HTTP_406_NOT_ACCEPTABLE)
        return super().create(request, *args, **kwargs)


@api_view()
def brew(request, drink):
    if drink != 'tea':
        return response.Response(
            {'info': "I'm a teapot"},
            status=status.HTTP_418_IM_A_TEAPOT)
    return response.Response({'info': 'Cup of tea!'},
                              status=status.HTTP_200_OK)
