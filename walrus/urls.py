from django.urls import path, include
from django.conf import settings
from rest_framework.routers import DefaultRouter
from . import views


app_name = 'walrus'


router = DefaultRouter()
router.register(r'todos', views.ToDoViewSet, basename='todo')


urlpatterns = [
    # The API version should probably be handled at server URL routing level
    path(f'api/v{settings.API_VERSION}/todos', views.ToDoList.as_view(), name='todos_list'),
    path(f'api/v{settings.API_VERSION}/todos/<int:pk>', views.ToDoView.as_view(), name='todos_view'),

    path(f'altapi/v{settings.API_VERSION}/', include(router.urls)),

    path('todos/<int:pk>', views.todos_per_user, name='todos_per_user'),
    path('todos/<str:username>', views.todos_per_user, name='todos_per_user'),
    # List to-dos for the currently logged-in user, if any
    path('todos', views.todos_per_user, name='todos_per_user'),

    path('brew/<str:drink>/', views.brew, name='brew'),
]
