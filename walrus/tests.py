from datetime import datetime, timezone, timedelta
from dateutil.parser import isoparse
from requests.auth import HTTPBasicAuth
from django.test import TestCase
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework import status
from rest_framework.test import RequestsClient
from .models import ToDo


CEST = timezone(timedelta(hours=1))

User = get_user_model()


class ToDoTest(TestCase):

    fixtures = ['todos.json', 'users.json']

    # We're using Django fixture files, but we can also use the standard unit-test
    # fixture setup, by commenting out the above line, and uncommenting the below function
    #def setUp(self):
    #    evert = User.objects.create_user(username='evert', password='rol')
    #    okke = User.objects.create_user(username='okke', password='formsma')
    #    wouter = User.objects.create_user(username='wouter', password='bongers')
    #    todo = ToDo(user=evert, title='assignment 1', completion_date=datetime(2021, 9, 29, tzinfo=CEST))
    #    todo.save()
    #    todo = ToDo(user=evert, title='assignment 2', completion_date=datetime(2021, 9, 29, tzinfo=CEST))
    #    todo.save()
    #    todo = ToDo(user=evert, title='assignment 3')
    #    todo.save()
    #    todo = ToDo(user=okke, title='evaluation')
    #    todo.save()

    def test_assignment3(self):
        todos = ToDo.objects.filter(user__username='evert')
        self.assertEqual(todos.count(), 3)

        todos = ToDo.objects.filter(user__username='okke')
        self.assertEqual(todos.count(), 1)

        todos = ToDo.objects.filter(user__username='wouter')
        self.assertEqual(todos.count(), 0)

        with self.assertRaises(User.DoesNotExist):
            User.objects.get(username='typo')

        # For an unknown user, an empty queryset is returned
        todos = ToDo.objects.filter(user__username='typo')
        self.assertEqual(todos.count(), 0)

        # Retrieve completed to-dos per user
        completed = {}
        for user in User.objects.all():
            #completed[user.username] = ToDo.objects.filter(user=user).filter(completion_date__isnull=False).count()
            completed[user.username] = ToDo.objects.filter(Q(user=user) & Q(completion_date__isnull=False)).count()
        self.assertDictEqual(completed, {'superuser': 0, 'evert': 2, 'wouter': 0, 'okke': 0})

        # Only list users with a positive number of completed todos.
        # Add one extra todo, so that there at least two users with completed todos.
        ToDo.objects.create(user=User.objects.get(username='wouter'), title='testing',
                            completion_date=datetime.now(CEST))
        from django.db.models import Count
        result = ToDo.objects.filter(completion_date__isnull=False)\
                             .values('user')\
                             .order_by('user')\
                             .annotate(completed=Count('user'))
        expected = [{'user': 2, 'completed': 2}, {'user': 3, 'completed': 1}]
        for item, expect in zip(result, expected):
            self.assertDictEqual(item, expect)


    def test_assignment7_permission(self):
        URL = f'http://localhost:8000/altapi/v{settings.API_VERSION}/todos/'

        # Use the RequestsClient, to mimick closer real-world usage
        client = RequestsClient()
        response = client.get(URL)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        client.auth = HTTPBasicAuth('nobody', 'allowed')
        response = client.get(URL)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        results = [{'id': 1, 'title': 'Assignment 1', 'completion_date': '2021-09-29T16:03:32Z',
                    'days_since_completion': 0, 'user': 2},
                   {'id': 2, 'title': 'Assignment 2', 'completion_date': '2021-09-29T16:52:55Z',
                    'days_since_completion': 0, 'user': 2},
                   {'id': 6, 'title': 'Assignment 3', 'completion_date': None,
                    'days_since_completion': -1, 'user': 2}]
        # Adjust `days_since_completion` dynamically
        # There is a bug here, that if there is a millisecond difference where the
        # number of completed days could differ by one between the local calculation
        # and that on the server
        now = datetime.now(CEST)
        for result in results:
            if result['days_since_completion']  >= 0:
                result['days_since_completion'] = (now - isoparse(result['completion_date'])).days

        client.auth = HTTPBasicAuth('evert', 'rol')
        response = client.get(URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for item, result in zip(response.json(), results):
            self.assertDictEqual(item, result)

        response = client.get(URL + '1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.json(), results[0])

        response = client.get(URL + '3/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        data = {"title": "A new to-do", "user": 2}
        response = client.post(URL, json=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = response.json()
        pk = data.pop('id')
        self.assertDictEqual(data, {'title': 'A new to-do', 'completion_date': None,
                                    'days_since_completion': -1, 'user': 2})

        data = {"title": "An updated title", "user": 2}
        response = client.put(URL + f'{pk}/', json=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response2 = client.get(URL + f'{pk}/')
        self.assertEqual(response2.status_code, status.HTTP_200_OK)
        data.update({'id': pk, 'completion_date': None, 'days_since_completion': -1, 'user': 2})
        self.assertDictEqual(data, response.json())
        self.assertDictEqual(data, response2.json())

        # log in as someone else, trying to add a to-do for user #2 and update & delete the previous entry

        client.auth = HTTPBasicAuth('okke', 'formsma')
        response = client.get(URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1)

        data = {"title": "A new to-do", "user": 2}
        response = client.post(URL, json=data)
        self.assertEqual(response.status_code, status.HTTP_406_NOT_ACCEPTABLE)
        data = {"title": "An updated title", "user": 2}
        response = client.put(URL + f'{pk}/', json=data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        response = client.delete(URL + f'{pk}/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # delete as original author
        client.auth = HTTPBasicAuth('evert', 'rol')
        response = client.delete(URL + f'{pk}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


    def test_assignment7_filtering(self):
        URL = f'http://localhost:8000/altapi/v{settings.API_VERSION}/todos/'

        # Use the RequestsClient, to mimick closer real-world usage
        client = RequestsClient()
        client.auth = HTTPBasicAuth('evert', 'rol')
        response = client.get(URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 3)

        response = client.get(URL + '?status=complete')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        self.assertEqual(len(data), 2)
        for item in data:
            self.assertIsNotNone(item['completion_date'])
            self.assertNotEqual(item['days_since_completion'], -1)

        response = client.get(URL + '?status=incomplete')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        self.assertEqual(len(data), 1)
        for item in data:
            self.assertIsNone(item['completion_date'])
            self.assertEqual(item['days_since_completion'], -1)
