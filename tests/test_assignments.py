"""Solutions in the form of tests to assignments 1 and 2

There is a bit of a cheat, since the parsing functions for assignment
2 should be in their own module or package, instead of directly
here. This is simplicity's sake, of course.

"""

import json


def parse_geojson(item, coordlist):
    """Recursively parse a nested dict and find the coordinate elements for the geo type"""

    # Note: we don't need to return a parsed child, or from this function itself
    # since `coordlist` is a list and thus mutates along the way

    if item.get('type') == 'geo':
        coords = item.get('coords')
        # Put some restrictions on the coordinate type
        # The simple test would be whether `coords` is `None`
        if isinstance(coords, list) and len(coords) == 2:
            coordlist.append(coords)
    children = item.get('children', [])
    # Parse children explicitly
    for child in children:
        parse_geojson(child, coordlist)


def parse_jsonstring(jsonstring):
    """Return a coordinate list from a JSON string"""

    data = json.loads(jsonstring)

    coordlist = []
    for value in data.values():
        parse_geojson(value, coordlist)

    return coordlist


def test_assignment1():
    """Reverse a list and join the reversed list with spaces"""
    words = ["be", "to", "not", "or", "be", "to"]
    solution = "to be or not to be"

    result = ' '.join(reversed(words))

    assert result == solution


def test_assignment2():
    """Parse json and make a list of all coordinates attached to type geo objects."""

    jsonstring = """{"foo": {"type": "geo", "coords": [2, 3], "children":
[{"type": "geo", "coords": [4, 1], "children": [{"type": "bar", "children":
[{"type": "geo", "coords": [8, 4]}]}, {"type": "geo", "coords": [3, 2]}]}]}}"""
    solution = [[2,3], [4,1], [8,4], [3,2]]

    coordlist = parse_jsonstring(jsonstring)

    assert coordlist == solution
