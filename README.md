# tinQwise assignments

This project contains a set of tinQwise Python coding assignments. The
actual assignments are listed verbatim at the bottom of this document.

The assignments have been subdivided into git commit steps (with some
intermediate steps): retracing the commits, should show the
assignments one by one.

## Assignment 1 & 2

The first two can be viewed in the assignments_1_2_ipynb`.  GitLab
should render the notebook automatically.

A more programmatic approach to these two assignments can be found as
unit test in the `tests/test_assignments.py` file. These can be run
with e.g. `pytest`.

## Assignment 3 -- 7

Assignments 3 to 7 have been worked into a Django app called
`walrus`. The app and dependencies can be installed using poetry,
which has been used to manage the dependencies. The easiest is
probably to use the virtual environment with poetry, with `poetry
shell`. Alternatively, install the dependencies yourself (there is
only four, and one optional), set the PYTHONPATH to include the
`walrus` directory and run a Django (development) server.

The required dependencies are:

* Django (3.2)
* djangorestframework (3.12)
* requests (2.26)
* python-dateutil (2.8)
* pytest (6.2; optional)

Python version 3.9 was used for development, but I expect earlier
versions such as 3.8 and 3.7 to also be able to run the app and tests
as well.

For the app to properly run, one will need to set up a Django
projectc, add `'rest_framework'` and `'walrus'` to the
`INSTALLED_APPS`. The HTML template (used in assignment 3) does rely
on a project-wide `base.html` template that needs to be available.


There are a few fixtures available for the tests; these contains dummy
users, one superuser (mainly for use with the Django admin site) and a
set of todo items pre-generated.

Assignment 3 is implemented in the view, as well as in a app unit test
(thus not the regular unit tests, but the Django/app-specific
one). Because of an initial misreading, the actual assignment is in
the last part of the `test_assignment3`: the number of completed todos
per available user.


## Quick tests

Assignments 4 to 7 can also be easily tested on the command line using
`curl`, as an alternative to the web interface. For example, for an
anonymous user (pre assignment 5), obtain a list of todos:

	curl 'http://localhost:8000/api/v1.0/todos'

With a user and password:

	curl -u evert:rol 'http://localhost:8000/api/v1.0/todos'

or with the viewset urls:

	curl -u evert:rol 'http://localhost:8000/altapi/v1.0/todos/'

(note the trailing slash here)

Produce formatted output by piping the result through `jq`:

	curl -u evert:rol 'http://localhost:8000/altapi/v1.0/todos/' | jq .

Include the HTTP status code:

	curl -w "%{http_code}" 'http://localhost:8000/altapi/v1.0/todos/'

(This then can't be piped through `jq`.)

Create (POST) a new to-do:

	curl -u evert:rol -X POST -H "Content-Type: application/json" -d '{"title": "A new to-do", "user": 2}' 'http://localhost:8000/altapi/v1.0/todos/'

Create a new to-do and automatically get the new primary key (id):

	id=$(curl -u evert:rol -X POST -H "Content-Type: application/json" -d '{"title": "A new to-do", "user": 2}' 'http://localhost:8000/altapi/v1.0/todos/' | jq .id)


Update (PUT) the previous to-do (given the $id):

	curl -u evert:rol -X PUT -H "Content-Type: application/json" -d '{"title": "An updated title", "user": 2}' "http://localhost:8000/altapi/v1.0/todos/${id}/"

Retrieve (GET) this to-do:

	curl -u evert:rol "http://localhost:8000/altapi/v1.0/todos/${id}/" | jq .

Remove (DELETE) the to-do:

	curl -u evert:rol -X DELETE "http://localhost:8000/altapi/v1.0/todos/${id}/"




## Assignments (verbatim)

```
Assignment 1

Reverse list and join list with space:

l = ["be", "to", "not", "or", "be", "to"]

Solution: to be or not to be



Assignment 2

Parse json and make a list of all coordinates attached to type geo objects



data = '{"foo": {"type": "geo", "coords": [2, 3], "children": [{"type": "geo", "coords": [4, 1], "children": [{"type": "bar", "children": [{"type": "geo", "coords": [8, 4]}]}, {"type": "geo", "coords": [3, 2]}]}]}}'



Solution: [[2,3], [4,1], [8,4], [3,2]]



Assignment 3

Create Todo model that stores a completion date (datetime), a user (Foreign Key), and a title field (Char). Efficiently retrieve the number of completed todo's per user.



Assignment 4

Based on the above Todo create a view (CRUD) and serializer in DRF (django rest framework). The serializer should output all fields and an extra field that ouputs the number of days since completion.



Assignment 5

Use permissions to make sure only the user doing the request can view their todo's.



Assignment 6

Add a filter on viewset so that users can retrieve only their incompleted todo's.



Assignment 7

Create a tests for assignment 5 and 6
```
